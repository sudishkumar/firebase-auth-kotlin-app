package com.famouscoader.firebaseauth

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.widget.AppCompatButton
import com.google.android.material.textfield.TextInputEditText
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore

class MainActivity : AppCompatActivity() {

    lateinit var auth : FirebaseAuth


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

         auth = FirebaseAuth.getInstance()

        val registerBtn = findViewById<AppCompatButton>(R.id.registerBtn)
        val loginBtn = findViewById<TextView>(R.id.login_txt)
        val emailEditText = findViewById<TextInputEditText>(R.id.email_textInputEditText)
        val passwordEditText = findViewById<TextInputEditText>(R.id.password_textInputEditText)

        registerBtn.setOnClickListener {
            auth.createUserWithEmailAndPassword(emailEditText.text.toString(), passwordEditText.text.toString())
                .addOnSuccessListener {

                    val userId = it.user?.uid

                    val  intent = Intent(this, HomeActivity::class.java)
                    startActivity(intent)

                    Toast.makeText(this, "You are Registered", Toast.LENGTH_SHORT).show()
                    finish()
                }
                .addOnFailureListener {
                    Toast.makeText(this, "" + it.message, Toast.LENGTH_SHORT).show()

                }
        }
        loginBtn.setOnClickListener {

            auth.signInWithEmailAndPassword(emailEditText.text.toString(), passwordEditText.text.toString())
                .addOnSuccessListener {

                    val userId = it.user?.uid

                    val  intent = Intent(this, HomeActivity::class.java)
                    startActivity(intent)

                    Toast.makeText(this, " Login successfully ", Toast.LENGTH_SHORT).show()
                    finish()
                }
                .addOnFailureListener {
                    Toast.makeText(this, "" + it.message, Toast.LENGTH_SHORT).show()

                }
        }
    }


}